º
let formulari = document.forms["form"];
console.log(document.forms["form"]);
let inputName = formulari["name"];
let inputSurname = formulari["surname"];
let inputEmail = formulari["email"];
let inputTelefon = formulari["telefon"];
let inputPassword1 = formulari["password1"];
let inputPassword2 = formulari["password2"];

formulari.addEventListener("submit", validaForm);
inputName.addEventListener("input", validaName)
inputSurname.addEventListener("input", validaSurname);
inputEmail.addEventListener("input", validaEmail);
inputTelefon.addEventListener("input", validaTelefon);
inputPassword1.addEventListener("input", validaPassword1);
inputPassword2.addEventListener("blur", validaPassword2);
document.getElementById("submit").addEventListener("click", enviarFormulario);


var nombre = document.getElementById('name');
var nombreError = document.getElementById('nameError');
const pattern = new RegExp('^[A-Z]+$', 'i');
const patternTelefon = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);


function validaForm(evt) {
    let form_valid = true;
    if (!validaTextEx()) {
        form_valid = false;
    }

    if (!form_valid) {
        evt.preventDefault();
    }
}


function validaName() {
    if (inputName.value == "") {//aquest if els he posat perque quant no hi hagi res desaparegui
        inputName.setCustomValidity('');
        inputName.style.borderColor = "unset";
        inputName.nextElementSibling.innerHTML = "";
        inputName.style.boxShadow = "";
        return;
    }
    inputName.setCustomValidity('');
    inputName.style.borderColor = "unset";
    //inputName.style.boxShadow="none";
    if (!inputName.reportValidity()) {
        //no es valid
        if (inputName.validity.tooShort) {
            inputName.setCustomValidity("minim 3 caracters");
            inputName.nextElementSibling.innerHTML = "error min 3 caracters";
            inputName.style.borderColor = "red";
            inputName.style.boxShadow = "red 0px 0px 0px 3px";
        }
        if (inputName.validity.tooLong) {
            inputName.setCustomValidity("maximo 10 caracters");
            inputName.nextElementSibling.innerHTML = "error max 10 caracters";
            inputName.style.borderColor = "red";
            inputName.style.boxShadow = "red 0px 0px 0px 3px";

        }
        return false;
    }
    if (!pattern.test(inputName.value)) {
        inputName.setCustomValidity("Only letters");
        inputName.nextElementSibling.innerHTML = "Only letters";
        inputName.style.borderColor = "red";
        inputName.style.boxShadow = "red 0px 0px 0px 3px";
        return false;
    }
    inputName.style.boxShadow = "green 0px 0px 0px 3px";
    inputName.style.borderColor = "green";
    inputName.nextElementSibling.innerHTML = "";


    return true;
}

function validaSurname() {
    if (inputSurname.value == "") {
        inputSurname.setCustomValidity('');
        inputSurname.style.borderColor = "unset";
        inputSurname.nextElementSibling.innerHTML = "";
        inputSurname.style.boxShadow = "";
        return true;
    }
    inputSurname.setCustomValidity('');
    inputSurname.style.borderColor = "unset";
    // inputName.style.boxShadow="none";
    if (!inputSurname.reportValidity()) {
        //no es valid
        if (inputSurname.validity.tooShort) {
            inputSurname.setCustomValidity("minim 3 caracters");
            inputSurname.nextElementSibling.innerHTML = "error min 3 caracters";
            inputSurname.style.borderColor = "red";
            inputSurname.style.boxShadow = "red 0px 0px 0px 3px";
        }
        if (inputSurname.validity.tooLong) {
            inputSurname.setCustomValidity("maximo 30 caracters");
            inputSurname.nextElementSibling.innerHTML = "error max 30 caracters";
            inputSurname.style.boxShadow = "red 0px 0px 0px 3px";
            inputSurname.style.borderColor = "red";

        }
        return false;
    }
    if (!pattern.test(inputSurname.value)) {
        inputSurname.setCustomValidity("Only letters");
        inputSurname.nextElementSibling.innerHTML = "Only letters";
        inputSurname.style.borderColor = "red";
        return false;
    }
    inputSurname.style.borderColor = "green";
    inputSurname.style.boxShadow = "green 0px 0px 0px 3px";
    inputSurname.nextElementSibling.innerHTML = "";

    return true;
}


const validateEmail = (email) => {
    return email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};


function validaEmail() {
    if (inputEmail.value == "") {
        inputEmail.setCustomValidity('');
        inputEmail.style.borderColor = "unset";
        inputEmail.nextElementSibling.innerHTML = "";
        inputEmail.style.boxShadow = "";
        return;
    }
    console.log(inputEmail.value);
    inputEmail.setCustomValidity('');
    if (!validateEmail(inputEmail.value)) {
        inputEmail.setCustomValidity('Incorrect email');
        inputEmail.nextElementSibling.innerHTML = "Incorrect email";
        inputEmail.style.borderColor = "red";
        inputEmail.style.boxShadow = "red 0px 0px 0px 3px";
        return false;
    } else {
        inputEmail.setCustomValidity("");
        inputEmail.nextElementSibling.innerHTML = "";
        inputEmail.style.borderColor = "green";
        inputEmail.style.boxShadow = "green 0px 0px 0px 3px";
        return true;
    }
}

function validaTelefon() {
    if (inputTelefon.value == "") {
        inputTelefon.setCustomValidity('');
        inputTelefon.style.borderColor = "unset";
        inputTelefon.nextElementSibling.innerHTML = "";
        inputTelefon.style.boxShadow = "";
        return;
    }
    if (patternTelefon.test(inputTelefon.value)) {
        inputTelefon.setCustomValidity('');
        inputTelefon.nextElementSibling.innerHTML = "";
        console.log("valid");
        inputTelefon.style.borderColor = "green";
        inputTelefon.style.boxShadow = "green 0px 0px 0px 3px";
        return true;
    } else {
        inputTelefon.setCustomValidity('Incorrect numero Telefono');
        inputTelefon.nextElementSibling.innerHTML = "Incorrect numero Telefono";
        inputTelefon.style.borderColor = "red";
        inputTelefon.style.boxShadow = "red 0px 0px 0px 3px";
        return false;
    }
}

function validaPassword1() {
    if (inputPassword1.value == "") {
        inputPassword1.setCustomValidity('');
        inputPassword1.style.borderColor = "unset";
        inputPassword1.nextElementSibling.innerHTML = "";
        inputPassword1.style.boxShadow = "";
        return;
    }
    inputPassword1.setCustomValidity('');
    inputPassword1.style.borderColor = "unset";
    if (!inputPassword1.reportValidity()) {
        //no es valid
        if (inputPassword1.validity.tooShort) {
            inputPassword1.setCustomValidity("minim 4 caracters");
            inputPassword1.nextElementSibling.innerHTML = "error min 4 caracters";
            inputPassword1.style.borderColor = "red";
            inputPassword1.style.boxShadow = "red 0px 0px 0px 3px";
        }
        if (inputPassword1.validity.tooLong) {
            inputPassword1.setCustomValidity("maximo 10 caracters");
            inputPassword1.nextElementSibling.innerHTML = "error max 10 caracters";
            inputPassword1.style.borderColor = "red";
            inputPassword1.style.boxShadow = "red 0px 0px 0px 3px";

        }
        return false;
    }
    inputPassword1.setCustomValidity("");
    inputPassword1.nextElementSibling.innerHTML = "";
    inputPassword1.style.boxShadow = "green 0px 0px 0px 3px";
    inputPassword1.style.borderColor = "green";

    return true;
}


function validaPassword2() {
    if (inputPassword2.value == "") {
        inputPassword2.setCustomValidity('');
        inputPassword2.style.borderColor = "unset";
        inputPassword2.nextElementSibling.innerHTML = "";
        inputPassword2.style.boxShadow = "";
        return;
    }
    console.log("funciona");
    if (inputPassword2.value == "") {
        inputPassword2.setCustomValidity("no puede estar vacio");
        inputPassword2.nextElementSibling.innerHTML = "no puede estar vacio";
        inputPassword2.style.borderColor = "red";
        inputPassword2.style.boxShadow = "red 0px 0px 0px 3px";
        return false;
    }
    if (inputPassword2.value != inputPassword1.value) {
        inputPassword2.setCustomValidity("deben coincidir");
        inputPassword2.nextElementSibling.innerHTML = "deben coincidir";
        inputPassword2.style.borderColor = "red";
        inputPassword2.style.boxShadow = "red 0px 0px 0px 3px";
        return false;
    }

    inputPassword2.setCustomValidity("");
    inputPassword2.nextElementSibling.innerHTML = "";
    inputPassword2.style.borderColor = "green";
    inputPassword2.style.boxShadow = "green 0px 0px 0px 3px";
    return true;



}


function enviarFormulario(e) {
    e.preventDefault();
    var html = ""
    if (validaName()) {
        html = html + "Nombre Válido"
    } else {
        html = html + "Nombre NO Válido: " + inputName.validationMessage
    }
    html = html + "\n"

    if (validaSurname()) {
        html = html + "Apellido Válido"
    } else {
        html = html + "Apellido NO Válido: " + inputSurname.validationMessage
    }
    html = html + "\n"

    if (validaEmail()) {
        html = html + "Email Válido"
    } else {
        html = html + "Email NO Válido: " + inputEmail.validationMessage
    }
    html = html + "\n"

    if (validaTelefon()) {
        html = html + "Telefono Válido"
    } else {
        html = html + "Telefono NO Válido: " + inputTelefon.validationMessage
    }
    html = html + "\n"

    if (validaPassword1()) {
        html = html + "Contrasenya Válido"
    } else {
        html = html + "Contrasenya NO Válido: " + inputPassword1.validationMessage
    }
    html = html + "\n"

    if (validaPassword2()) {
        html = html + "Contrasenya 2 Válido"
    } else {
        html = html + "Contrasenya 2 NO Válido: " + inputPassword2.validationMessage
    }
    html = html + "\n"

    document.getElementById("textArea").innerHTML = html
    return
}