import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopHeadClientComponent } from './top-head-client.component';

describe('TopHeadClientComponent', () => {
  let component: TopHeadClientComponent;
  let fixture: ComponentFixture<TopHeadClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopHeadClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopHeadClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
