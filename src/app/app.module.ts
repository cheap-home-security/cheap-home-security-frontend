import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { FormularioComponent } from './formulario/formulario.component';
import { IniciarSessionComponent } from './iniciar-session/iniciar-session.component';
import { InicioComponent } from './inicio/inicio.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AboutUsComponent } from './about-us/about-us.component';
import { FAQComponent } from './faq/faq.component';
import { TopHeadComponent } from './top-head/top-head.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { CarouselComponent } from './carousel/carousel.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatToolbarModule} from "@angular/material/toolbar";
import { PanelComponent } from './panel/panel.component';
import { FooterClientComponent } from './footer-client/footer-client.component';
import { TopHeadClientComponent } from './top-head-client/top-head-client.component';
import { PerfilClienteComponent } from './perfil-cliente/perfil-cliente.component';
import { CamaraComponent } from './camara/camara.component';


@NgModule({
  declarations: [
    AppComponent,
    PaginaPrincipalComponent,
    FormularioComponent,
    IniciarSessionComponent,
    InicioComponent,
    AboutUsComponent,
    FAQComponent,
    InicioComponent,
    TopHeadComponent,
    FooterComponent,
    CarouselComponent,
    PanelComponent,
    FooterClientComponent,
    TopHeadClientComponent,
    PerfilClienteComponent,
    CamaraComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    NgbModule,
    MatToolbarModule,



  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
