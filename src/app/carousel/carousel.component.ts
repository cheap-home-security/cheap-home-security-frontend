import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngbd-app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
 
  images = ['ejemplo2.jpg', 'ejemplo3.jpeg', 'ejemplo1.png'].map((n) => `../assets/media/Inicio/${n}`);
  
  constructor() { }

  ngOnInit(): void {
  }

}
