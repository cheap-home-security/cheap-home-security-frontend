import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';



@Component({
  selector: 'app-camara',
  templateUrl: './camara.component.html',
  styleUrls: ['./camara.component.scss']
})

export class CamaraComponent implements OnInit {

  key:any = undefined;
  constructor() { }

  ngOnInit(): void {
    // @ts-ignore
    const firebaseConfig = {
      apiKey: "AIzaSyD_Whxp-90LaON-VythJBA4ujLGaw2ds2E",
      authDomain: "chsserver-448fc.firebaseapp.com",
      databaseURL: "https://chsserver-448fc-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "chsserver-448fc",
      storageBucket: "chsserver-448fc.appspot.com",
      messagingSenderId: "238568654605",
      appId: "1:238568654605:web:e1da5739a1e7737141d244",
      measurementId: "G-GK192FXLJK"
    };

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    const firestore = firebase.firestore();


    const server = {
      iceServers: [
        {
          urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
        },
      ],
      iceCandidatePoolSize: 10,
    }

    const pc = new RTCPeerConnection(server);
    let localStream: any = null;
    // let localStream2: any = null;
    // let remoteStream: any = null;

    const webcamButton: any = document.getElementById('webcamButton');
    const webcamVideo: any = document.getElementById('webcamVideo');
    const callInput: any = document.getElementById('callInput');



    webcamButton.onclick = async () => {
      localStream = await navigator.mediaDevices.getUserMedia({video: true, audio: true});
      // remoteStream = new MediaStream();

      localStream.getTracks().forEach((track: any) => {
        pc.addTrack(track, localStream);
      });

      // localStream2 = await navigator.mediaDevices.getUserMedia({video: true, audio: false});

      // webcamVideo.srcObject = localStream2;
      webcamVideo.srcObject = localStream;
      webcamVideo.srcObject.muted = true;
      webcamButton.disabled = true;
        // Reference Firestore collections for signaling
        const callDoc = firestore.collection('calls').doc();
        const offerCandidates = callDoc.collection('offerCandidates');
        const answerCandidates = callDoc.collection('answerCandidates');

        this.key = callDoc.id;
        // console.log(callInput.value, callDoc.id);

        pc.onicecandidate = (event) => {
          event.candidate && offerCandidates.add(event.candidate.toJSON());
        };

        const offerDescription = await pc.createOffer();
        await pc.setLocalDescription(offerDescription);

        const offer = {
          sdp: offerDescription.sdp,
          type: offerDescription.type,
        };

        await callDoc.set({offer});

        callDoc.onSnapshot((snapshot:any) => {
          const data = snapshot.data();
          if (!pc.currentRemoteDescription && data?.answer) {
            const answerDescription = new RTCSessionDescription(data.answer);
            pc.setRemoteDescription(answerDescription);
          }
        });

        answerCandidates.onSnapshot((snapshot:any) => {
          snapshot.docChanges().forEach((change:any) => {
            if (change.type === 'added') {
              const candidate = new RTCIceCandidate(change.doc.data());
              pc.addIceCandidate(candidate);
            }
          });
        });

        // hangupButton.disabled = false;
      };
    };



}
