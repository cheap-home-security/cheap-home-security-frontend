import { FormularioComponent } from "../formulario/formulario.component";
import { PerfilClienteComponent } from "../perfil-cliente/perfil-cliente.component";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IniciarSessionComponent} from "../iniciar-session/iniciar-session.component";
import {ajaxPost} from "rxjs/internal-compatibility";


@Injectable()
export class userService {
  constructor(private http: HttpClient) {
    //ajaxPost():Observable<any>{
    //let url ="http://localhost/AuthUser.php";
    //return this.http.post(this.url,generateHeaders());
    // }

  }


  // private static url = 'http://127.0.0.1:8884';

  insertUser(formulario: FormularioComponent): String {//Observable<any>{

   this.http.post<any>('/api' + '/register',
      {
        'dni': formulario.dniVar,
        'name': formulario.nameVar,
        'surname': formulario.surnameVar,
        'pass': formulario.password1Var,
        'email': formulario.emailVar,
        'mobile': formulario.telefonoVar,
        'subs': formulario.subVar
      },
      this.generateHeaders()
    ) .subscribe(data => {
      if (data.code() == 200) {
        console.log("val")
      }
      if (data.code() == 409) {
        console.log("error email repetido")
      }


      //data = "Usuario registrado correctamente"
    }, error => {
      console.log('oops', error)
      if (error.error.code == 409) {
        formulario.formError = "El email ya está registrado"
      }
    })

    return "funciona"; //this.conexHttp.get(percistanceService.url,
    ///{headers:new HttpHeaders({"Content-Type": "application/json"})})
  }
  getUser(): any {
    return this.http.get<any>('/api' + '/user',
      this.generateHeaders()
    );
  }


  updateUser(perfilcliente: PerfilClienteComponent): String {//Observable<any>{

    this.http.post<any>('/api' + '/register',
      {
        'dni': perfilcliente.dniVar,
        'name': perfilcliente.nameVar,
        'surname': perfilcliente.surnameVar,
        'pass': perfilcliente.password1Var,
        'email': perfilcliente.emailVar,
        'mobile': perfilcliente.telefonoVar,
        'subs': perfilcliente.subVar

      },
      this.generateHeaders()
    ).subscribe(data => {

      if (data.code() == 200) {
        console.log("val")
      }
      if (data.code() == 409) {
        console.log("error email repetido")
      }


      //data = "Usuario registrado correctamente"
    }, error => {
      console.log('oops', error)
      if (error.error.code == 409) {
        perfilcliente.formError = "El email ya está registrado"
      }
    })

    return "funciona"; //this.conexHttp.get(percistanceService.url,
    ///{headers:new HttpHeaders({"Content-Type": "application/json"})})
  }

  loginUser(loginUser: IniciarSessionComponent): String {


    this.http.post<any>('/api' + '/login',
      {
        'email': loginUser.emailVar,
        'pass': loginUser.passwordVar,
      },
      this.generateHeaders()
    ).subscribe(respuesta => {
      console.log(respuesta.data.token)
      localStorage.setItem("token", respuesta.data.token)
      localStorage.setItem("refresh_token", respuesta.data.refreshToken)


    }, error => {
      console.log('oops', error)
      if (error.error.code == 409) {
        loginUser.loginError = "El email o la contrasenya son incorrectos"
      } else {
        loginUser.loginError = "Ha habido un error"
      }
    })

    return "funciona";
  }

  validarToken(): boolean{


    this.http.get<any>('/api'+'/info',
      this.generateHeaders()

    ).subscribe(respuesta=>{
      if(respuesta.code() == 200){
        console.log("val");
      }


  },
      error => {
        if (error.error.code == 500 || error.error.code == 401 || error.error.code == 406) {
          console.log("error")
          this


        }
        if (error.error.code == 403) {
          console.log("token expirado")
          this.validarRefreshToken()


        }

      }
  );
  return true;
  }

  validarRefreshToken():boolean{

    this.http.get<any>('/api'+'/loginrefresh',
      this.generateHeaders()

    ).subscribe(respuesta=>{
      if(respuesta.code() == 200){
        console.log("val");

        window.localStorage.removeItem('token')
        window.localStorage.removeItem('refresh_token')
        localStorage.setItem("token", respuesta.data.token)
        localStorage.setItem("refresh_token", respuesta.data.refreshToken)
      }
    },
      error => {
        window.localStorage.removeItem('token')
        window.localStorage.removeItem('refresh_token')

      }
    );
    return true
  }



 generateHeaders() {
  if (localStorage.getItem("token") && localStorage.getItem("token")!="undefined") {
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': localStorage.getItem("token")+" "+ localStorage.getItem('refresh_token') }) };
  } else { return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; }
}
}
