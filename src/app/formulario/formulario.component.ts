import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { userService } from '../service/userService.service';
import {Router} from "@angular/router";


import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from "rxjs";
import {isNullCheck} from "@angular/core/schematics/utils/typescript/nodes";
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
  providers: [userService]
})
export class FormularioComponent implements OnInit {


  nameVar: String = "";
  nameInit: Boolean = false
  surnameVar: String = "";
  surnameInit: Boolean = false
  emailVar: String = ""
  emailInit: Boolean = false
  dniVar: String = ""
  dniInit: Boolean = false
  telefonoVar: String = ""
  telefonoInit: Boolean = false
  dateVar: string = ""
  dateInit: Boolean = false
  password1Var: String = ""
  password1Init: Boolean = false
  password2Var: String = ""
  password2Init: Boolean = false
  subVar: String = ""
  subInit: Boolean = false
  formError: String = ""


  pattern = new RegExp('^[A-Z]+$', 'i');


  constructor(private _userService: userService, private router: Router) { }

  ngOnInit(): void {
  }


  onChangeName(event: String): void {
    this.nameInit = true
    this.nameVar = event
    //  var el = (document.getElementById("name") as HTMLInputElement)
    // el.setCustomValidity(this.isNameValid()[1]);
  }

  onChangeSurname(event: String): void {
    this.surnameInit = true
    this.surnameVar = event
  }

  onChangeEmail(event: String): void {
    this.emailInit = true
    this.emailVar = event
  }

  onChangeDni(event: String): void {
    this.dniInit = true
    this.dniVar = event
  }

  onChangeTelefono(event: String): void {
    this.telefonoInit = true
    this.telefonoVar = event
  }

  onChangeDate(event: string): void {
    this.dateInit = true
    this.dateVar = event
  }

  onChangePassword1(event: String): void {
    this.password1Init = true
    this.password1Var = event
  }

  onChangePassword2(event: String): void {
    this.password2Init = true
    this.password2Var = event
  }

  onChangeSub(event: String): void {
    this.subInit = true
    this.subVar = event
  }



  isNameValid(): [Boolean, string] {
    if (this.nameVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (this.nameVar.length < 3) {
      return [false, "Tiene que tener como mínimo 3 caracteres"]
    }
    if (!this.pattern.test(String(this.nameVar))) {
      return [false, "solo puede tener letras"]
    }
    return [true, ""]
    //Angular limita el maximo automaticamente asi que no hace falta controlarlo
  }

  isSurnameValid(): [Boolean, string] {
    if (this.surnameVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (this.surnameVar.length < 3) {
      return [false, "Tiene que tener como mínimo 3 caracteres"]
    }
    if (!this.pattern.test(String(this.surnameVar))) {
      return [false, "solo puede tener letras"]
    }
    return [true, ""]

  }


  isEmailValid(): [Boolean, string] {
    if (this.emailVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (!this.emailVar.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      return [false, "formato de email no correcto"]
    }

    return [true, ""]
  }

  isDniValid(): [Boolean, string] {
    if (this.dniVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (!this.dniVar.match(/^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i)) {
      return [false, "formato de Dni no correcto"]
    }
    return [true, ""]
  }

  isTelefonoValid(): [Boolean, string] {
    if (this.telefonoVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (!this.telefonoVar.match(/(\+34|0034|34)?[ -]*(6|7)[ -]*([0-9][ -]*){8}/)) {
      return [false, "formato de Telefono no correcto"]
    }
    return [true, ""]
  }



  isPassword1Valid(): [Boolean, string] {
    if (this.password1Var == "") {
      return [false, "No puede estar vacío"]
    }
    if (this.password1Var.length < 3) {
      return [false, "Tiene que tener como mínimo 3 caracteres"]
    }
    if (!this.pattern.test(String(this.password1Var))) {
      return [false, "solo puede tener letras"]
    }
    return [true, ""]
  }

  isPassword2Valid(): [Boolean, string] {
    if (this.password2Var == "") {
      return [false, "No puede estar vacío"]
    }
    if (this.password1Var != this.password2Var) {
      return [false, "Tienen que coicidir"]
    }
    return [true, ""]
  }

  isDateValid(): [Boolean, string] {
    if (this.dateVar == "") {
      return [false, "No puede estar vacío"]
    }
    let hoy: Date = new Date();

    let fechaNacimiento: Date = new Date(this.dateVar)

    let edad = hoy.getFullYear() - fechaNacimiento.getFullYear()
    let diferenciaMeses = hoy.getMonth() - fechaNacimiento.getMonth()
    if (
      diferenciaMeses < 0 ||
      (diferenciaMeses == 0 && hoy.getDate() < fechaNacimiento.getDate())
    ) {
      edad--
    }

    if (edad < 18) {
      return [false, "Tienes que ser mayor de edad"]
    }

    return [true, ""]
  }


  isSubValid(): [Boolean, string]{
    if(this.subVar == ""){
      return [false, "Se debe selecionar una subscripcion"]
    }

    return [true, ""]
  }


  formSubmit(event: any) {
    this.subInit = true
    if (!this.isSubValid()[0] || !this.isNameValid()[0] || !this.isSurnameValid()[0] || !this.isEmailValid()[0] || !this.isTelefonoValid()[0] || !this.isDateValid()[0] || !this.isPassword1Valid()[0] || !this.isPassword2Valid()[0] || !this.isDniValid()[0]) {
      console.log("falta validacion")

      event.stopPropagation();
      return
    }

    this.router.navigate(['iniciarSession'])
    this._userService.insertUser(this)

  }
}
