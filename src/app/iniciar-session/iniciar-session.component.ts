import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {userService} from "../service/userService.service";
import {Router, RouterModule} from "@angular/router";

@Component({
  selector: 'app-iniciar-session',
  templateUrl: './iniciar-session.component.html',
  styleUrls: ['./iniciar-session.component.scss'],
  providers:[userService]
})
export class IniciarSessionComponent implements OnInit {

  emailVar: String = "";
  emailInit: Boolean = false;
  passwordVar: String = "";
  passwordInit: Boolean = false;
  loginError: String = "";

  constructor(private _userService: userService, private router: Router) { }

  ngOnInit(): void {
    console.log(window.localStorage.getItem('token'));

    if (window.localStorage.getItem('token') == undefined || window.localStorage.getItem('token') == null) {
      this.router.navigate(['iniciarSession'])
    } else {
      if (this._userService.validarToken()) {

        this.router.navigate(['panel'])
      }

    }
  }

  onChangeEmail(event: String): void{
    this.emailInit = true;
    this.emailVar = event;
  }

  onChangePassword(event: String): void{
    this.passwordInit = true;
    this.passwordVar = event;
  }

  isEmailValid(): [Boolean, string] {
    if (this.emailVar == "") {
      return [false, "No puede estar vacío"]
    }
    if(!this.emailVar.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
      return [false, "formato de email no correcto"]
    }

    return [true, ""]
  }

  isPasswordValid(): [Boolean, string]{
    if (this.passwordVar == "") {
      return [false, "No puede estar vacío"]
    }
    if (this.passwordVar.length < 3) {
      return [false, "Tiene que tener como mínimo 3 caracteres"]
    }
    return [true, ""]
  }

  enviarAPanel(){
    this.router.navigate(['panel'])

  }

  loginSubmit(event: any) {
    console.log("aqui");
    if(!this.isEmailValid()[0] || !this.isPasswordValid()[0]){
      console.log("falta validacion");


      event.stopPropagation();

      return
    }
    this.router.navigate(['panel'])
    this._userService.loginUser(this)
    }

}
