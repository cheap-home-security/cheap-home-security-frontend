import {Component, OnInit} from '@angular/core';
// import {initializeApp} from "firebase/app";
// import {getAnalytics} from "firebase/analytics";
// import firebase from "firebase/compat";
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';




@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})


  export class PanelComponent implements OnInit {

    constructor() {
    }


    ngOnInit(): void {
      // @ts-ignore
      const firebaseConfig = {
        apiKey: "AIzaSyD_Whxp-90LaON-VythJBA4ujLGaw2ds2E",
        authDomain: "chsserver-448fc.firebaseapp.com",
        databaseURL: "https://chsserver-448fc-default-rtdb.europe-west1.firebasedatabase.app",
        projectId: "chsserver-448fc",
        storageBucket: "chsserver-448fc.appspot.com",
        messagingSenderId: "238568654605",
        appId: "1:238568654605:web:e1da5739a1e7737141d244",
        measurementId: "G-GK192FXLJK"
      };

      if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
      }
      const firestore = firebase.firestore();

// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
// const firesore = firestore(app);

      const server = {
        iceServers: [
          {
            urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
          },
        ],
        iceCandidatePoolSize: 10,
      }

      const pc = new RTCPeerConnection(server);
      let remoteStream: any = null;


      const callInput: any = document.getElementById('callInput');
      const answerButton: any = document.getElementById('answerButton');
      const remoteVideo: any = document.getElementById('remoteVideo');

      remoteStream = new MediaStream();

      pc.ontrack = (event:any) => {
        event.streams[0].getTracks().forEach((track:any) => {
          remoteStream.addTrack(track);
        });
      };
      remoteVideo.srcObject = remoteStream;


      answerButton.onclick = async () => {
        const callId = callInput.value;
        const callDoc = firestore.collection('calls').doc(callId);
        const answerCandidates = callDoc.collection('answerCandidates');
        const offerCandidates = callDoc.collection('offerCandidates');
        console.log("log",callId, callDoc, answerCandidates, offerCandidates);
        pc.onicecandidate = (event:any) => {
          event.candidate && answerCandidates.add(event.candidate.toJSON());
        };

        const callData = (await callDoc.get()).data();
        // @ts-ignore
        const offerDescription = callData.offer;
        await pc.setRemoteDescription(new RTCSessionDescription(offerDescription));
        const answerDescription = await pc.createAnswer();
        await pc.setLocalDescription(answerDescription);

        const answer = {
          type: answerDescription.type,
          sdp: answerDescription.sdp,
        };

        await callDoc.update({answer});

        callDoc.onSnapshot((snapshot:any) => {
          const data = snapshot.data();
          if (!pc.currentRemoteDescription && data?.answer) {
            const answerDescription = new RTCSessionDescription(data.answer);
            pc.setRemoteDescription(answerDescription);
          }
        });

        offerCandidates.onSnapshot((snapshot:any) => {
          snapshot.docChanges().forEach((change:any) => {
            console.log(change);
            if (change.type === 'added') {
              let data = change.doc.data();
              pc.addIceCandidate(new RTCIceCandidate(data));
            }
          });
        });
      };

    }

  }



