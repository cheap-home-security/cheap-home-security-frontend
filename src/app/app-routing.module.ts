import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { FormularioComponent } from './formulario/formulario.component';
import { IniciarSessionComponent } from './iniciar-session/iniciar-session.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FAQComponent } from './faq/faq.component';
import {PanelComponent} from "./panel/panel.component";
import { PerfilClienteComponent } from './perfil-cliente/perfil-cliente.component';
import {CamaraComponent} from "./camara/camara.component";

const routes: Routes = [
  {path:'',component:PaginaPrincipalComponent},
  {path:'formulario', component:FormularioComponent},
  {path:'iniciarSession', component:IniciarSessionComponent},
  {path:'aboutUs', component:AboutUsComponent},
  {path:'faq', component:FAQComponent},
  {path:'panel', component:PanelComponent},
  {path:'camara',component:CamaraComponent},
  {path:'perfil', component:PerfilClienteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
